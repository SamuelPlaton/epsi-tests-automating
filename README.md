# epsi-tests-automating

## Introduction

The project contains data and minigames around countries
(Flag Quiz, Hangman, Countries details), it implements e2e tests and advanced CI.

## Installation
```
# Install dependencies
npm install
# Build our project
npm run build
# Run project
npm run serve
```

## Code structure

Here is our code structure explained : 
- assets: Our app assets such as style or icons.
- components: The components composing our pages (Navigation, Form ...).
- router: Our VueRouter handling redirections and page display depending on the url.
- views: Our app pages.

## Environment Variables

The project needs environment variables to work, check the .env.example file for more details.

## Tests

Our project implement [Cypress](https://www.cypress.io/), an e2e testing framework.
It allow us to test each feature of our app and make sure there is no regression.  
The tests files can be found in the ``cypress/integration`` directory
```
# Launch server first
npm run serve
# Launch tests without a web interface
npm run cypress:run
# Or launch tests within a web browser
npm run cypress:open
```

## Linter

To help us having strict code style and conventions, we use the next packages :  
- [Eslint](https://www.npmjs.com/package/eslint) : A tool to help us identifying code patterns
and fixing incorrect ones, the Airbus code conventions are used on this project.
- [Prettier](https://www.npmjs.com/package/prettier) : An optionated code formatter to enforce a consistent style.
It's working in pair with Eslint.
- [Husky](https://www.npmjs.com/package/husky): By improving our commits by setting pre-commit hooks that can trigger eslint.

Here are the commands to trigger Eslint : 
```
# Run linter and display errors and warnings
npm run lint
# Run linter and display errors and warnings, correct them when it's possible
npm run lint:fix
```

## Continuous Integration

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is the solution used in this project. Our CI script include several steps :  
- build: Installing and building the package.
- test: Testing the lint rules and the e2e tests.
- sonarcloud-check: Calling the [Sonar Cloud](https://sonarcloud.io/) service to review the source code
  (Bugs, Vulnerabilities, Code smells, Coverage ...).

## Continuous Development

For our continuous development, we use [Vercel](https://vercel.com/dashboard) to deploy our app in the multiple environments.  
Our git flow is separated in 3 environments : 
- [master](https://epsi-tests-automating-master.vercel.app/) : Our production environment.
- [develop](https://epsi-tests-automating-develop.vercel.app/): Our pre-production / staging environment.
- [features](https://epsi-tests-automating-features.vercel.app/): Our intern environment made for development.  

Furthermore, each iteration of our app (through commits in merge requests)
are also deployed and hosted in Vercel for testing purpose.

## Gitlab

The main project is hosted in [Gitlab](https://about.gitlab.com/).  
The main repository can be found [here](https://gitlab.com/SamuelPlaton/epsi-tests-automating).  
Gitlab allows us to use Strict conventions and rules as Merge Requests or branch protection rules.

## Other conventions

Other project and code conventions are setup in intern such as [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary).
