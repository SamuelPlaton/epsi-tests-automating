describe('Currency Converter', () => {
  beforeEach(() => {
    cy.visit('/curr-converter');
  });
  it('Display default form informations', () => {
    // check for base currency
    cy.get('label[for="base"]');
    cy.get('select[name="base"]').should('have.value', null);
    // check for converted currency
    cy.get('label[for="converted"]');
    cy.get('select[name="converted"]').should('have.value', null);
    // input value
    cy.get('input[name="base-value"]').should('have.value', 0);
    cy.contains('Result : 0.00');
  });
  it('Can select currencies', () => {
    // select values
    cy.get('select[name="base"]').select('EUR');
    cy.get('select[name="converted"]').select('USD');
    // check that values are not null anymore
    cy.get('select[name="base"]').should('not.have.value', null);
    cy.get('select[name="converted"]').should('not.have.value', null);
  });
  it('Provides currency value security', () => {
    // Check that only numbers can be entered
    cy.get('input[name="base-value"]').type('text @./*&é"');
    cy.get('input[name="base-value"]').should('have.value', 0);
  });
  it('Can convert values', () => {
    // select values
    cy.get('select[name="base"]').select('EUR');
    cy.get('select[name="converted"]').select('USD');
    // Check that value conversion work
    cy.get('input[name="base-value"]').clear().type(50);
    cy.contains('Result : 55.00'); // Check that 50€ is correctly converted to 56.50$
  });
  it('Can update currencies and values', () => {
    // set default values
    cy.get('select[name="base"]').select('EUR');
    cy.get('select[name="converted"]').select('USD');
    cy.get('input[name="base-value"]').clear().type(50);
    // update base currency
    cy.get('select[name="base"]').select('AED');
    cy.contains('Result : 13.10');
    // Update value
    cy.get('input[name="base-value"]').clear().type(1000);
    cy.contains('Result : 261.90');
  });
  it('Can reset values', () => {
    // set default values
    cy.get('select[name="base"]').select('EUR');
    cy.get('select[name="converted"]').select('USD');
    cy.get('input[name="base-value"]').clear().type(50);
    // reset values
    cy.get('input[name="base-value"]').clear();
    cy.contains('Result : 0.00');
  });
});
