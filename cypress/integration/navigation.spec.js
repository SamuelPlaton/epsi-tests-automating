describe('Navigation Test', () => {
  beforeEach(() => cy.visit('/'));
  it('Visits Home', () => {
    cy.contains('Home').click();
    cy.url().should('include', 'http://localhost:8080/#/');
    cy.get('a[href*="/"]').should('have.class', 'router-link-exact-active');
  });
  it('Visits List', () => {
    cy.contains('List').click();
    cy.url().should('include', 'http://localhost:8080/#/list');
    cy.get('a[href*="/list"]').should('have.class', 'router-link-exact-active');
  });
  it('Visits Quiz', () => {
    cy.contains('Quiz').click();
    cy.url().should('include', 'http://localhost:8080/#/quiz');
    cy.get('a[href*="/quiz"]').should('have.class', 'router-link-exact-active');
  });
  it('Visits Hangman', () => {
    cy.contains('Hangman').click();
    cy.url().should('include', 'http://localhost:8080/#/hangman');
    cy.get('a[href*="/hangman"]').should('have.class', 'router-link-exact-active');
  });
  it('Visits Currency Converter', () => {
    cy.contains('Currency Converter').click();
    cy.url().should('eq', 'http://localhost:8080/#/curr-converter');
    cy.get('a[href*="/curr-converter"]').should('have.class', 'router-link-exact-active');
  });
});
