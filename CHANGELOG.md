# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://github.com/mokkapps/changelog-generator-demo/compare/v0.1.0...v0.1.1) (2022-01-11)


### Features

* setup changelog generation ([7746cd8](https://github.com/mokkapps/changelog-generator-demo/commits/7746cd81ca4225345895f94eec58fd793e6bba2b))
* test ([76652c6](https://github.com/mokkapps/changelog-generator-demo/commits/76652c614a9191078422df078f3f125f0705aed7))
* update-readme ([9a17c95](https://github.com/mokkapps/changelog-generator-demo/commits/9a17c9578dfce1f1cacf27a4a110253ccda6e58e))


### Bug Fixes

* fix readme wrong line ([ca25659](https://github.com/mokkapps/changelog-generator-demo/commits/ca256590a2c22ba1833db4ec89c5dd11a9a839b0))

## 0.1.0 (2022-01-11)


### Features

* add currency converter route ([bd6d609](https://github.com/mokkapps/changelog-generator-demo/commits/bd6d609a712076b5077a74611f6170bbe2a03d25))
* add detail page ([af917aa](https://github.com/mokkapps/changelog-generator-demo/commits/af917aa27ceb0294e31e9278647cb0bec46c04f8))
* add home page and header ([67f8554](https://github.com/mokkapps/changelog-generator-demo/commits/67f85549130014a617113105495cb23ca7a75d98))
* add quiz flag component ([7a33691](https://github.com/mokkapps/changelog-generator-demo/commits/7a33691bb1cc9ae6ec3709f334570c423928e1d5))
* add test route ([d3fedd2](https://github.com/mokkapps/changelog-generator-demo/commits/d3fedd26f9f4b377f4c28e0020fc830be0dfdbeb))
* build project ([d9e0857](https://github.com/mokkapps/changelog-generator-demo/commits/d9e0857b98c4271e0c043a8bdf9ea057d4a52f12))
* finished hangman gamemode ([68e3177](https://github.com/mokkapps/changelog-generator-demo/commits/68e3177957290d3cb9b8917c0aa0d5c3098a16fc))
* finished list page ([843553f](https://github.com/mokkapps/changelog-generator-demo/commits/843553fe488a12a9dbb100fb1d0ecbcbe9e3c08e))
* init list functionnality ([835e4d7](https://github.com/mokkapps/changelog-generator-demo/commits/835e4d70bdf139b62762c46142e9dcbe8b19054d))


### Bug Fixes

* add docs directory ([40e8a12](https://github.com/mokkapps/changelog-generator-demo/commits/40e8a12fb3b7db700ce5be19dddb40105003bb65))
* cleanup default project ([8456b93](https://github.com/mokkapps/changelog-generator-demo/commits/8456b933a16e0d7984366e2c0f2de43e8830b27c))
* sonar cloud code smell ([40aa8ee](https://github.com/mokkapps/changelog-generator-demo/commits/40aa8eed07b4644f2568a7a29ef9d17f36e981ca))
* test currency converter ([12c3085](https://github.com/mokkapps/changelog-generator-demo/commits/12c3085fcb6fcd93a88bb57f746f2d5a733c7cbc))
* update countries api version ([f46025a](https://github.com/mokkapps/changelog-generator-demo/commits/f46025a32fa41c3de597d42c61f51fd9bfd74078))
* update countries api version ([6d96ea9](https://github.com/mokkapps/changelog-generator-demo/commits/6d96ea9a18d174bae94c2f7c4a9ef2f7eee12e7b))
* update countries api version ([c3fbb0e](https://github.com/mokkapps/changelog-generator-demo/commits/c3fbb0ea31b33208c6eb07999f7b197cd0ce7d20))


### Tests

* currency converter ([a4a963d](https://github.com/mokkapps/changelog-generator-demo/commits/a4a963d4a159e26391d20338933932142e44139d))


* update readme ([663817c](https://github.com/mokkapps/changelog-generator-demo/commits/663817cec21988a1547e8cc471340d9a9f504190))

## 0.1.0 (2022-01-11)


### Features

* add currency converter route ([bd6d609](https://github.com/mokkapps/changelog-generator-demo/commits/bd6d609a712076b5077a74611f6170bbe2a03d25))
* add detail page ([af917aa](https://github.com/mokkapps/changelog-generator-demo/commits/af917aa27ceb0294e31e9278647cb0bec46c04f8))
* add home page and header ([67f8554](https://github.com/mokkapps/changelog-generator-demo/commits/67f85549130014a617113105495cb23ca7a75d98))
* add quiz flag component ([7a33691](https://github.com/mokkapps/changelog-generator-demo/commits/7a33691bb1cc9ae6ec3709f334570c423928e1d5))
* add test route ([d3fedd2](https://github.com/mokkapps/changelog-generator-demo/commits/d3fedd26f9f4b377f4c28e0020fc830be0dfdbeb))
* build project ([d9e0857](https://github.com/mokkapps/changelog-generator-demo/commits/d9e0857b98c4271e0c043a8bdf9ea057d4a52f12))
* finished hangman gamemode ([68e3177](https://github.com/mokkapps/changelog-generator-demo/commits/68e3177957290d3cb9b8917c0aa0d5c3098a16fc))
* finished list page ([843553f](https://github.com/mokkapps/changelog-generator-demo/commits/843553fe488a12a9dbb100fb1d0ecbcbe9e3c08e))
* init list functionnality ([835e4d7](https://github.com/mokkapps/changelog-generator-demo/commits/835e4d70bdf139b62762c46142e9dcbe8b19054d))


### Bug Fixes

* add docs directory ([40e8a12](https://github.com/mokkapps/changelog-generator-demo/commits/40e8a12fb3b7db700ce5be19dddb40105003bb65))
* cleanup default project ([8456b93](https://github.com/mokkapps/changelog-generator-demo/commits/8456b933a16e0d7984366e2c0f2de43e8830b27c))
* sonar cloud code smell ([40aa8ee](https://github.com/mokkapps/changelog-generator-demo/commits/40aa8eed07b4644f2568a7a29ef9d17f36e981ca))
* test currency converter ([12c3085](https://github.com/mokkapps/changelog-generator-demo/commits/12c3085fcb6fcd93a88bb57f746f2d5a733c7cbc))
* update countries api version ([f46025a](https://github.com/mokkapps/changelog-generator-demo/commits/f46025a32fa41c3de597d42c61f51fd9bfd74078))
* update countries api version ([6d96ea9](https://github.com/mokkapps/changelog-generator-demo/commits/6d96ea9a18d174bae94c2f7c4a9ef2f7eee12e7b))
* update countries api version ([c3fbb0e](https://github.com/mokkapps/changelog-generator-demo/commits/c3fbb0ea31b33208c6eb07999f7b197cd0ce7d20))


### Tests

* currency converter ([a4a963d](https://github.com/mokkapps/changelog-generator-demo/commits/a4a963d4a159e26391d20338933932142e44139d))


* update readme ([663817c](https://github.com/mokkapps/changelog-generator-demo/commits/663817cec21988a1547e8cc471340d9a9f504190))


